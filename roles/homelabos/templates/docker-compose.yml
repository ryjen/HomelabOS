# Commented out services are in beta testing and not integrated or documented yet. Uncomment them at your own peril.

version: '3'

services:

  apple_health_influx:
    image: nickbusey/healthdata_influx:cron
    volumes:
      - /var/homelabos/nextcloud/data/{{ apple_health_nextcloud_username }}/files/export/:/export/
      - /var/homelabos/apple_health_influx/config.yml:/config.yml

  bitwarden_web:
    image: mprasil/bitwarden
    restart: always
    volumes:
      - /var/homelabos/bitwarden:/data
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:warden.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.tor.frontend.rule=Host:warden.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  convos:
    image: nordaaker/convos
    restart: always
    volumes:
     - /var/homelabos/convos:/data
    ports:
      - 8033:3000
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:irc.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=3000"
      - "traefik.tor.frontend.rule=Host:irc.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=3000"

  # couchpotato:
  #   image: "couchpotato/couchpotato"
  #   volumes:
  #     - /var/homelabos/couchpotato/data:/datadir
        # - /mnt/nas/Movies:/media
  #   ports:
  #     - "5050:5050"
  #   restart: always
  #   links:
  #     - transmission
  #     - jackett
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:couchpotato.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=5050"

  darksky-influx:
    image: erwinsteffens/darksky-influxdb:latest
    restart: always
    links:
      - influxdb
    environment:
      - DARKSKY_KEY={{ darksky_key }}
      # This is every 2 minutes. This is about as fast as you can go with the free API keys without running out of queries.
      - CRON=0 */2 * * * *
      - INFLUXDB_HOST=influxdb
      - INFLUXDB_DATABASE=darksky
      - DARKSKY_LATITUDE={{ latitude }}
      - DARKSKY_LONGITUDE={{ longitude }}

  dasher:
    image: hijinx/dasher
    restart: always
    network_mode: host
    volumes:
      - /var/homelabos/dasher/config.json:/usr/src/app/config/config.json

  docs:
    image: kyma/docker-nginx
    volumes:
      - /var/homelabos/docs/site:/var/www
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:docs.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.tor.frontend.rule=Host:docs.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  emby:
    image: emby/embyserver:latest
    restart: always
    volumes:
      - /var/homelabos/emby:/config
      - /mnt/nas:/mnt/nas
      - /mnt/nas/tmp:/config/transcoding-temp
    ports:
      - 8096:8096
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:emby.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=8096"
      - "traefik.tor.frontend.rule=Host:emby.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=8096"

  firefly_iii_app:
    environment:
      - FF_DB_HOST=firefly_iii_db
      - FF_DB_NAME=firefly_db
      - FF_DB_USER=firefly_db
      - FF_DB_PASSWORD=firefly_db_secret
      - FF_APP_KEY=S0m3R@nd0mStr1ngOf31Ch@rsEx@ctly
      - FF_APP_ENV=local
      - TZ={{ timezone }}
      - ServerName=money.{{ domain }}
    image: jc5x/firefly-iii
    links:
      - firefly_iii_db
    ports:
      - "8555:80"
    volumes:
      - /var/homelabos/firefly/export:/var/www/firefly-iii/storage/export
      - /var/homelabos/firefly/upload:/var/www/firefly-iii/storage/upload
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:money.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.tor.frontend.rule=Host:money.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  firefly_iii_db:
    environment:
      - MYSQL_DATABASE=firefly_db
      - MYSQL_USER=firefly_db
      - MYSQL_PASSWORD=firefly_db_secret
      - MYSQL_RANDOM_ROOT_PASSWORD=yes
    image: "mariadb:latest"
    volumes:
      - /var/homelabos/firefly/db:/var/lib/mysql

  # freshrss:
  #   image: linuxserver/freshrss
  #   ports:
  #     - 8084:80
  #   volumes:
  #     - /var/homelabos/freshrss:/config
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:rss.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=80"

  gitea:
    image: gitea/gitea:latest
    environment:
      - USER_UID=1000
      - USER_GID=1000
    restart: always
    links:
      - gitea_db:db
    volumes:
      - /var/lab/homelabos/gitea:/data
    ports:
      - "3030:3000"
      - "222:22"
    depends_on:
      - gitea_db
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:git.{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=3000"
      - "traefik.tor.frontend.rule=Host:git.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=3000"

  gitea_db:
    image: mariadb
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=gitea
      - MYSQL_USER=gitea
      - MYSQL_PASSWORD=gitea
      - MYSQL_DATABASE=gitea
    volumes:
      - /var/homelabos/gitea_db:/var/lib/mysql

  grafana:
    image: grafana/grafana
    restart: always
    links:
      - influxdb
    ports:
      - 3000:3000
    volumes:
      - /var/homelabos/grafana:/var/lib/grafana
    environment:
      - GF_INSTALL_PLUGINS=grafana-clock-panel,natel-discrete-panel,petrslavotinek-carpetplot-panel,vonage-status-panel,raintank-worldping-app
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:grafana.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=3000"
      - "traefik.tor.frontend.rule=Host:grafana.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=3000"

  homeassistant:
    image: homeassistant/home-assistant
    volumes:
      - /var/homelabos/homeassistant:/config
      - /etc/localtime:/etc/localtime:ro
      - /etc/letsencrypt:/etc/letsencrypt
    restart: always
    ports:
      - 8123:8123
      - 1883:1883
      # - 8080:8080
      - 51827:51827
    links:
      - influxdb
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:homeassistant.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=8123"
      - "traefik.tor.frontend.rule=Host:homeassistant.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=8123"

  # huginn:
  #   image: huginn/huginn
  #   links:
  #     - huginn_db:mysql
  #   ports:
  #     - 3132:3000
  #   environment:
  #     - HUGINN_DATABASE_NAME=huginn
  #     - HUGINN_DATABASE_USERNAME=huginn
  #     - HUGINN_DATABASE_PASSWORD=somethingsecret
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:huginn.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=3000"

  # huginn_db:
  #   image: mariadb
  #   restart: always
  #   volumes:
  #     - /var/homelabos/huginn_db:/var/lib/mysql
  #   environment:
  #     - MYSQL_ROOT_PASSWORD=huginn
  #     - MYSQL_PASSWORD=somethingsecret
  #     - MYSQL_DATABASE=huginn
  #     - MYSQL_USER=huginn

  influxdb:
    image: influxdb
    restart: always
    volumes:
      - /var/homelabos/influxdb:/var/lib/influxdb
    ports:
      - 8086:8086

  # jackett:
  #   image: linuxserver/jackett
  #   restart: always
  #   volumes: 
  #     - /var/homelabos/jackett/config:/config
  #     - /var/homelabos/jackett/downloads:/downloads
  #     - /etc/localtime:/etc/localtime:ro
  #   environment:
  #     - TZ={{ timezone }}
  #   ports:
  #     - 9117:9117

  mastodon_db:
    restart: always
    image: postgres:9.6-alpine
    volumes:
      - /var/homelabos/mastodon/postgres:/var/lib/postgresql/data

  mastodon_redis:
    restart: always
    image: redis:4.0-alpine
    volumes:
      - /var/homelabos/mastodon/redis:/data

  mastodon_es:
    restart: always
    image: docker.elastic.co/elasticsearch/elasticsearch-oss:6.1.3
    environment:
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
    volumes:
      - /var/homelabos/mastodon/elasticsearch:/usr/share/elasticsearch/data

  mastodon_web:
    image: tootsuite/mastodon
    restart: always
    env_file: /var/homelabos/mastodon/mastodon.env
    command: bash -c "rm -f /mastodon/tmp/pids/server.pid; bundle exec rails s -p 3000 -b '0.0.0.0'"
    ports:
      - "3456:3000"
    depends_on:
      - mastodon_db
      - mastodon_redis
      - mastodon_es
    volumes:
      - /var/homelabos/mastodon/public/assets:/mastodon/public/assets
      - /var/homelabos/mastodon/public/packs:/mastodon/public/packs
      - /var/homelabos/mastodon/public/system:/mastodon/public/system
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:mastodon.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=3000"

  mastodon_streaming:
    image: tootsuite/mastodon
    restart: always
    env_file: /var/homelabos/mastodon/mastodon.env
    command: yarn start
    ports:
      - "127.0.0.1:4000:4000"
    depends_on:
      - mastodon_db
      - mastodon_redis

  mastodon_sidekiq:
    image: tootsuite/mastodon
    restart: always
    env_file: /var/homelabos/mastodon/mastodon.env
    command: bundle exec sidekiq -q default -q mailers -q pull -q push
    depends_on:
      - mastodon_db
      - mastodon_redis
    volumes:
      - /var/homelabos/mastodon/public/packs:/mastodon/public/packs
      - /var/homelabos/mastodon/public/system:/mastodon/public/system

  matomo_db:
   image: mariadb:latest
   volumes:
     - /var/homelabos/matomo/db:/var/lib/mysql
   environment:
     - MYSQL_ROOT_PASSWORD=matomo
     - MYSQL_USER=matomo
     - MYSQL_PASSWORD=matomo
     - MYSQL_DATABASE=matomo

  matomo_app:
    image: crazymax/matomo:latest
    links:
      - matomo_db:db
    volumes:
      - /var/homelabos/matomo/data:/data
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:matomo.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.tor.frontend.rule=Host:matomo.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  minio:
    image: minio/minio
    command: server /data
    volumes:
      - /mnt/nas/Backups/minio:/data
      - /var/homelabos/minio/config:/root/.minio/
    ports:
      - 9110:9000
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:minio.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=9000"
      - "traefik.tor.frontend.rule=Host:minio.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=9000"

  # monicahq:
  #   image: monicahq/monicahq
  #   links:
  #     - monicahq_db
  #   ports:
  #     - 8176:80
  #   volumes:
  #     - /var/homelabos/monica/storage/app/public:/var/www/monica/storage/app/public
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:monica.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=80"

  # monicahq_db:
  #   image: mysql:5.7
  #   environment:
  #     - MYSQL_ROOT_PASSWORD=sekret_root_password
  #     - MYSQL_DATABASE=monica
  #     - MYSQL_USER=homestead
  #     - MYSQL_PASSWORD=secret
  #   volumes:
  #     - /var/homelabos/monica/mysql:/var/lib/mysql

  nextcloud:
    image: nextcloud
    restart: always
    ports:
      - 8181:80
    links:
      - nextcloud_db
    volumes:
      - /var/homelabos/nextcloud:/var/www/html
    restart: always
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:nextcloud.{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.tor.frontend.rule=Host:nextcloud.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  nextcloud_db:
    image: mariadb
    restart: always
    volumes:
      - /var/homelabos/nextcloud_db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=nextcloud
      - MYSQL_PASSWORD=nextcloud
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud

  # nzbget:
  #   image: linuxserver/nzbget
  #   ports:
  #     - 6789:6789
  #   environment:
  #     - TZ={{ timezone }}
  #   volumes:
  #     - /var/homelabos/nzbget:/config
  #     - /mnt/nas/Downloads:/downloads
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:nzbget.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=6789"

  openvpn:
    cap_add:
     - NET_ADMIN
    image: kylemanna/openvpn
    ports:
     - "1194:1194/udp"
    restart: always
    volumes:
     - /var/homelabos/openvpn:/etc/openvpn

  organizr:
    image: lsiocommunity/organizr
    restart: always
    ports:
      - 8083:80
    volumes:
      - /var/homelabos/organizr:/config
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.tor.frontend.rule=Host:{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  paperless:
    image: danielquinn/paperless
    restart: always
    ports:
      - "8325:8000"
    healthcheck:
      test: ["CMD", "curl" , "-f", "http://localhost:8000"]
      interval: 30s
      timeout: 10s
      retries: 5
    volumes:
      - /var/homelabos/paperless/data:/usr/src/paperless/data
      - /var/homelabos/paperless/media:/usr/src/paperless/media
      - /mnt/nas/Documents/consume:/consume
      - /mnt/nas/Documents/export:/export
    environment:
      - PAPERLESS_OCR_LANGUAGES=
      - PAPERLESS_PASSPHRASE=homelabos
    command: ["runserver", "--insecure", "--noreload", "0.0.0.0:8000"]
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:paperless.{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=8000"
      - "traefik.tor.frontend.rule=Host:paperless.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=8000"

  paperless_consumer:
    image: danielquinn/paperless
    restart: always
    depends_on:
      - paperless
    volumes:
      - /var/homelabos/paperless/data:/usr/src/paperless/data
      - /var/homelabos/paperless/media:/usr/src/paperless/media
      - /mnt/nas/Documents/consume:/consume
      - /mnt/nas/Documents/export:/export
    environment:
      - PAPERLESS_PASSPHRASE=homelabos
    command: ["document_consumer"]

  pihole:
    image: diginc/pi-hole:latest
    restart: always
    volumes:
      - /var/homelabos/pihole/config/:/etc/pihole/
      - /var/homelabos/pihole/dnsmasq.d/:/etc/dnsmasq.d/
    environment:
      - WEBPASSWORD={{ default_password }}
      - VIRTUAL_HOST=pihole.{{ domain }}
      # - ServerIP={{ ansible_ssh_host }}
      # - ServerIP="${IP:-$(ip route get 8.8.8.8 | awk '{ print $NF; exit }')}"
      # - ServerIPv6="${IPv6:-$(ip -6 route get 2001:4860:4860::8888 | awk '{ print $10; exit }')}"
    ports:
      - 53:53/tcp
      - 53:53/udp
      - 8765:80
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:pihole.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=80"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.tor.frontend.rule=Host:pihole.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=80"

  portainer:
    image: portainer/portainer
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/homelabos/portainer:/data
    ports:
      - 9000:9000
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:docker.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=9000"
      - "traefik.tor.frontend.rule=Host:docker.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=9000"

  # sonarr:
  #   image: linuxserver/sonarr
  #   ports:
  #     - 8989:8989
  #   environment:
  #     - PGID=1000
  #     - PUID=1000
  #     - TZ={{ timezone }}
  #   links:
  #     - jackett
  #     - transmission
  #   volumes:
  #     - /etc/localtime:/etc/localtime:ro
  #     - /var/homelabos/sonarr/config:/config
  #     - /mnt/nas/TV:/tv
  #     - /var/homelabos/sonarr/downloads:/downloads
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:sonarr.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=8989"

  # https://nickbusey.gitlab.io/HomelabOS/software/restic/
  restic:
    image: lobaro/restic-backup-docker:v1.0
    environment:
      - RESTIC_REPOSITORY={{ s3_path }}
      - AWS_ACCESS_KEY_ID={{ s3_access_key }}
      - AWS_SECRET_ACCESS_KEY={{ s3_secret_key }}
      - RESTIC_PASSWORD={{ s3_backup_password }}
      - BACKUP_CRON={{ s3_backup_cron }}
      - HOSTNAME={{ domain }}
      - RESTIC_JOB_ARGS="--exclude=minio"
      - RESTIC_FORGET_ARGS="--prune --keep-last 10 --keep-hourly 24 --keep-daily 7 --keep-weekly 52 --keep-monthly 120 --keep-yearly 100"
    links:
      - minio
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/homelabos:/data:ro

  syncthing:
    image: linuxserver/syncthing
    volumes:
      - /var/homelabos/syncthing:/config
      - /mnt/nas/Syncthing:/data
    environment:
      - PGID=1004
      - PUID=1000
    ports:
      - 22000:22000
      - 21027:21027/udp
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:sync.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=8384"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM http://{{ domain }}"
      - "traefik.http.frontend.headers.customFrameOptionsValue=ALLOW-FROM https://{{ domain }}"
      - "traefik.tor.frontend.rule=Host:sync.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=8384"

  telegraf:
    image: telegraf
    restart: always
    volumes:
      - /var/homelabos/telegraf/telegraf.conf:/etc/telegraf/telegraf.conf
      - /var/run/docker.sock:/var/run/docker.sock
      - /sys:/rootfs/sys:ro
      - /proc:/rootfs/proc:ro
      - /etc:/rootfs/etc:ro
      - /mnt/nas:/mnt/nas
    links:
      - influxdb

  traefik:
    image: traefik
    restart: always
    ports:
      - "80:80"
      - "443:443"
      - "8080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - /var/homelabos/traefik/traefik.toml:/traefik.toml
      - /var/homelabos/traefik/acme.json:/acme.json

  # https://nickbusey.gitlab.io/HomelabOS/software/transmission/
  transmission:
    image: haugene/transmission-openvpn
    cap_add:
      - NET_ADMIN
    devices:
      - /dev/net/tun
    restart: always
    ports:
      - "9091:9091"
      - "8888:8888"
    dns:
      - 8.8.8.8
      - 8.8.4.4
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /var/homelabos/transmission/data:/data
      - /var/homelabos/transmission/config:/config
      - /mnt/nas/Downloads:/downloads
      - /var/homelabos/transmission/watch:/watch
    environment:
      - OPENVPN_PROVIDER=PIA
      - OPENVPN_USERNAME={{ openvpn_username }}
      - OPENVPN_PASSWORD={{ openvpn_password }}
      - OPENVPN_OPTS=--inactive 3600 --ping 10 --ping-exit 60
      - PGID=1000
      - PUID=1000
      - TZ={{ timezone }}
      - TRANSMISSION_RPC_AUTHENTICATION_REQUIRED=true
      # Password = `transmission`
      - TRANSMISSION_RPC_PASSWORD="{62b16db87b89a91dd49a5110a7cafc06d20eb4f2wtK6kqPj"
      - TRANSMISSION_RPC_USERNAME={{ default_username }}
    labels:
      - "traefik.enable=true"
      - "traefik.http.frontend.rule=Host:torrent.{{ domain }}"
      - "traefik.http.protocol={{ protocol }}"
      - "traefik.http.port=9091"
      - "traefik.tor.frontend.rule=Host:torrent.{{ tor_domain }}"
      - "traefik.tor.protocol={{ protocol }}"
      - "traefik.tor.port=9091"

  # urbackup:
  #   image: cfstras/urbackup
  #   ports:
  #     - 55413
  #     - 55414
  #     - 55415
  #     - 35622
  #   volumes:
  #     - /etc/localtime:/etc/localtime:ro
  #     - /var/homelabos/urbackup/db/:/var/urbackup
  #     - /var/homelabos/urbackup/backups/:/backup
  #   labels:
  #     - "traefik.enable=true"
  #     - "traefik.http.frontend.rule=Host:backup.{{ domain }}"
  #     - "traefik.http.protocol={{ protocol }}"
  #     - "traefik.http.port=55414"

  xfinityusageinfluxdb:
    restart: unless-stopped
    image: nickbusey/xfinityusageinfluxdb
    links:
      - influxdb
    environment:
      - XFINITY_USER={{ xfinity_user }}
      - XFINITY_PASSWORD={{ xfinity_password }}
      - INFLUXDB_HOST=influxdb